from flask import Flask,render_template,request,redirect,flash

from models import db,UserModel
app=Flask(__name__)
app.secret_key="ABC"
# Db Config
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///userdata.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db.init_app(app)

@app.before_first_request
def create_table():
    db.create_all()

# Home Page
@app.route("/")
def home_route():
    users=UserModel.query.all()
    return render_template("home.html",users=users)

# Add User To DB
@app.route('/create',methods=['POST'])
def create_user():
    if request.method=='POST':
        print(request.form)
        full_name=request.form['full_name']
        salary=request.form['salary']
        user=UserModel(full_name,salary)
        db.session.add(user)
        db.session.commit()
        flash(f"{user.full_name} Added To DB","success")  

        return redirect('/')

# Update User
@app.route("/update/<int:id>",methods=['GET','POST'])
def update_user(id):
    user=UserModel.query.filter_by(id=id).first()
    if request.method=="POST":
        user.full_name=request.form["full_name"]
        user.salary=request.form["salary"]
        db.session.add(user)
        db.session.commit()
        return redirect("/")


    return render_template('update_user.html',user=user)

    



@app.route("/delete/<int:id>",methods=['GET'])
def remove_user(id):
    user=UserModel.query.filter_by(id=id).first()
    if request.method=='GET':
        db.session.delete(user)
        db.session.commit()
        flash(f"{user.full_name} Removed From DB","warning")  

        return redirect("/")



if __name__=="__main__":
    app.run(debug=True)    