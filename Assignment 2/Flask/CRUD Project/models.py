from flask_sqlalchemy import SQLAlchemy

db=SQLAlchemy()
class UserModel(db.Model):
    __tablename__="usertable"

    id=db.Column(db.Integer,primary_key=True)
    full_name=db.Column(db.String())
    salary=db.Column(db.Integer())

    def __init__(self,full_name,salary) -> None:
        self.full_name=full_name
        self.salary=salary

    def __repr__(self) -> str:
        return f"{self.full_name} and {self.salary}"